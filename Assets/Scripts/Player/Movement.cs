using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Move Player
/// </summary>
public class Movement : MonoBehaviour {
    [SerializeField] private string horizontalInputName;
    [SerializeField] private string verticalInputName;
    [SerializeField] private float movementSpped;

    [SerializeField] private float slopeForce;
    [SerializeField] private float slopeForceRayLength;

    private CharacterController characterController;

    [SerializeField] private AnimationCurve jumpFallOff;
    [SerializeField] private float jumpMultiplier;
    [SerializeField] private KeyCode jumpKey;

    private bool isJumping;

    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }
    #region MonoBehaviourAPI
    private void Start()
    {
        
    }
    private void Update()
    {
        PlayerMovement();
    }
    private void PlayerMovement()
    {
        float horizInput = Input.GetAxis(horizontalInputName);
        float vertInput = Input.GetAxis(verticalInputName);

        Vector3 forwardMovement = transform.forward * vertInput;
        Vector3 rightMovement = transform.right * horizInput;

        characterController.SimpleMove(Vector3.ClampMagnitude(forwardMovement + rightMovement,1.0f)*movementSpped);

        if((vertInput !=0 || horizInput != 0) && OnSlope())
            characterController.Move(Vector3.down * characterController.height / 2*this.gameObject.transform.localScale.y * slopeForce * Time.deltaTime);

        JumpInput();

    }

    private bool OnSlope()
    {
        if (isJumping)
            return false;

        RaycastHit hit;

        if (Physics.Raycast(transform.position,Vector3.down,out hit,characterController.height/2 * this.gameObject.transform.localScale.y * slopeForceRayLength))
            if (hit.normal != Vector3.up)
                return true;
        return false;
    }

    private void JumpInput()
    {
        if (Input.GetKeyDown(jumpKey) && !isJumping)
        {
            isJumping = true;
            StartCoroutine(JumpEvent());
        }
    }
    private IEnumerator JumpEvent()
    {
        characterController.slopeLimit = 90.0f;
        float timeInAir = 0.0f;
        do
        {
            float jumpForce = jumpFallOff.Evaluate(timeInAir);
            characterController.Move(Vector3.up * jumpForce * jumpMultiplier*Time.deltaTime);
            timeInAir += Time.deltaTime;
            yield return null;
        }
        while (!characterController.isGrounded && characterController.collisionFlags !=CollisionFlags.Above);
        characterController.slopeLimit = 45.0f;
        isJumping = false;
    }
}
    #endregion