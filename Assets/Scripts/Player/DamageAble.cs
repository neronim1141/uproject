﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageAble : MonoBehaviour
{

    public float health=10;
    public GameObject RootObject;

    public void Update()
    {
        if (health <= 0)
        {
            Destroy(RootObject);
        }
    }
    public void Damage(float amount)
    {
        health-=amount;
    }
}
