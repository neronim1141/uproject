﻿using behaviorTree;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Drone : MonoBehaviour
{
    public GameObject bullet;
    //public GameObject Weapon;
    //public GameObject WhereToShoot;
    BlackBoard blackBoard;
    public NavMeshAgent droneAgent;
    // Start is called before the first frame update
    void Awake()
    {
       droneAgent = GetComponent<NavMeshAgent>();
       blackBoard= new BlackBoard(this.gameObject, "drone1");
    }
    
    // Update is called once per frame
    void Update()
    {
        BehaviorLibrary.DroneBehavior.Tick(blackBoard);
    }
}
