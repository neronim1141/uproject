﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceTest : MonoBehaviour
{
    public GameObject prefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject gm =Instantiate(prefab,this.transform.position,this.transform.rotation);
            Physics.IgnoreCollision(gm.GetComponent<Collider>(), transform.parent.GetComponent<Collider>());
        }
    }
}
