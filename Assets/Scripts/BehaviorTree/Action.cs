﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace behaviorTree
{
    public class Action : IBehavior
    {
        public Func<BlackBoard, Status> fn;
        public Action()
        {
        }
        public Action(Func<BlackBoard, Status> fn)
        {
            this.fn = fn;
        }
        public virtual Status Tick(BlackBoard blackBoard)
        {
            return fn(blackBoard);
        }
    }
}