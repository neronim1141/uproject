﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using behaviorTree;
public class GetNewPatrolPoint : IBehavior
{
    public Status Tick(BlackBoard blackBoard)
    {
        int index = UnityEngine.Random.Range(0,blackBoard.Points.Length);
        blackBoard.nextPoint = blackBoard.Points[index];
        if (blackBoard.nextPoint != null)
        {
            return Status.Success;
        }
        else
        {
            return Status.Failure;
        }
    }

    protected float SquaredDistance(Vector3 a, Vector3 b)
    {
        float x = a.x - b.x;
        float y = a.y - b.y;
        float z = a.z - b.z;
        return x * x + y * y + z * z;
    }
}
