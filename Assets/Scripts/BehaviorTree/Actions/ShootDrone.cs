﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace behaviorTree
{
    public class ShootDrone : IBehavior
    {
        public Status Tick(BlackBoard blackBoard)
        {

            blackBoard.Timer += Time.deltaTime;
            Debug.Log(blackBoard.Timer.ToString());
            if (blackBoard.Timer >= 0.5f)
            {
                GameObject bullet = GameObject.Instantiate(blackBoard.Holder.gameObject.GetComponent<Drone>().bullet, new Vector3(blackBoard.Holder.gameObject.transform.position.x, blackBoard.Holder.gameObject.transform.position.y + 1.0f, blackBoard.Holder.gameObject.transform.position.z + 2.0f), blackBoard.Holder.gameObject.transform.rotation);
                bullet.GetComponent<Rigidbody>().AddForce(Vector3.forward * 600.0f);
                Debug.Log("Szczelam");
                blackBoard.Timer = 0.0f;
                return Status.Success;
            }
            else
            {
                return Status.Failure;
            }

        }
    }
}