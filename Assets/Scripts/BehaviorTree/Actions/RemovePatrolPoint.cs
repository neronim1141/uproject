﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using behaviorTree;
public class RemovePatrolPoint : IBehavior
{
    public Status Tick(BlackBoard blackBoard)
    {
        NavMeshAgent agent = blackBoard.Holder.gameObject.GetComponent<NavMeshAgent>();
        if (blackBoard.nextPoint != null)
        {

            blackBoard.nextPoint = null;
            agent.isStopped = true;
           
        }
        return Status.Success;
    }
}
