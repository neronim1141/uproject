﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using behaviorTree;
public class MoveToPoint : IBehavior
{
    public Status Tick(BlackBoard blackBoard)
    {
        GameObject pointToGo = blackBoard.nextPoint;
        NavMeshAgent agent = blackBoard.Holder.gameObject.GetComponent<NavMeshAgent>();

        if (pointToGo != null)
        {
            agent.isStopped = false;
            agent.SetDestination(pointToGo.transform.position);
            return Status.Success;
        }
        else
        {
            return Status.Failure;
        }
    }
}
