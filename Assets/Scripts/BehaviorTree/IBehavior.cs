﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace behaviorTree
{
    public interface IBehavior
    {
        Status Tick(BlackBoard blackBoard);

    }
}

