﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace behaviorTree
{
    public class Composite : IComposite, IBehavior
    {
        Status status { get; }
        protected List<IBehavior> Children { get; set; }
        public Composite(params IBehavior[] behaviors)
        {
            this.Children = behaviors.ToList<IBehavior>();
        }
        public virtual Status Tick(BlackBoard blackBoard) { throw new NotImplementedException(); }

        public void Add(IBehavior child)
        {
            this.Children.Add(child);
        }
    }
}