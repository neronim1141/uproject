﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace behaviorTree
{
    public class HasPatrolPoint : Condition
    {
        protected override bool Check(BlackBoard blackBoard)
        {
            return blackBoard.nextPoint != null;
        }
    }
}
