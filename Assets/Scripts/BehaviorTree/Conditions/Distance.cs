﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace behaviorTree
{
    public class Distance : Condition
    {
        protected float dist;

        public Distance(float dist)
        {
            this.dist = dist;

        }
        protected virtual float MinDistance(BlackBoard blackBoard)
        {
            throw new System.NotImplementedException();

        }
        protected override bool Check(BlackBoard blackBoard)
        {
            return MinDistance(blackBoard) < (dist * dist);
        }
        protected float SquaredDistance(Vector3 a, Vector3 b)
        {
            float x = a.x - b.x;
            float y = a.y - b.y;
            float z = a.z - b.z;
            float sum = x * x + y * y + z * z;
            //Debug.Log(sum.ToString());
            return sum;
        }
    }
}
