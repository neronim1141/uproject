﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace behaviorTree
{
    public class IsNearPatrolPoint : Distance
    {
        public IsNearPatrolPoint(float dist) : base(dist)
        {

        }
        protected override float MinDistance(BlackBoard blackBoard)
        {
            Vector3 Holder = blackBoard.Holder.transform.position;

            return SquaredDistance(blackBoard.nextPoint.transform.position, Holder);

        }
    }
}