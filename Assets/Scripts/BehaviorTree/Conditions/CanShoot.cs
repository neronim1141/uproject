﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace behaviorTree {
    public class CanShoot : Condition
    {
        public new Status Tick(BlackBoard blackBoard)
        {
            Timer value = new Timer(2.0f);
            if (!blackBoard.timerDict.TryGetValue("CanShoot", out value))
            {
                blackBoard.timerDict.Add("CanShoot",value);
            }
            if (value.updateTime(0.0f))
            {
                return Status.Success;
            }
            else
            {
                return Status.Failure;
            }
        }
    }
}
