﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace behaviorTree
{
    public class SeePlayer : Condition
    {
        protected override bool Check(BlackBoard blackBoard)
        {
            return true;
        }
        public Vector3 DirFromAngle(BlackBoard blackBoard,float angleInDegrees,bool angleIsGlobal)
        {
            if (!angleIsGlobal)
            {
                angleInDegrees += blackBoard.Holder.transform.eulerAngles.y;
            }
            return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
        }
    }
}