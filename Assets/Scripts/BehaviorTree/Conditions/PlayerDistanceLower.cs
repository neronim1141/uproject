﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace behaviorTree
{
    public class PlayerDistanceLower : Distance
    {

        public PlayerDistanceLower(float dist) : base(dist)
        {

        }
        protected override float MinDistance(BlackBoard blackBoard)
        {
            Vector3 Holder = blackBoard.Holder.transform.position;
            float min = int.MaxValue;
            for (int i = 0; i < blackBoard.Players.Length; i++)
            {
                float d = SquaredDistance(blackBoard.Players[i].transform.position, Holder);
                if (d < min)
                {
                    min = d;
                    blackBoard.target = blackBoard.Players[i];
                }
            }

            return min;

        }
    }
}