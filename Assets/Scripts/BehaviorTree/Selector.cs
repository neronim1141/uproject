﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace behaviorTree
{
    public class Selector : Composite
    {
        public Selector(params IBehavior[] behaviors) : base(behaviors)
        {

        }
        public override Status Tick(BlackBoard blackBoard)
        {

            for (int i = 0; i < Children.Count; i++)
            {
                Status s = Children[i].Tick(blackBoard);
                if (s == Status.Running) return Status.Running;
                if (s == Status.Success) return Status.Success;

            }
            return Status.Failure;
        }

    }
}