﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace behaviorTree
{
    public enum Status
    {
        Success,
        Running,
        Failure
    }
}
