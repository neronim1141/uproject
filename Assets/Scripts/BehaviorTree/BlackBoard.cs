﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace behaviorTree
{
    public class BlackBoard
    {
        public Dictionary<string, Timer> timerDict = new Dictionary<string, Timer>();
        public float Timer = 0.0f;
        public string name;

        public bool playerInSight = false;
        public float fieldOfDroneViewAngle;
        public Vector3 personalLastSighting;

        [Range(0,360)]
        public float viewAngle;
        public float viewRadius;

        #region DroneView
        public LayerMask targetMask;
        public LayerMask obstacleMask;

        public Collider[] targetsInViewRadius;

        #endregion

        public GameObject Holder;
        public PlayerSimple[] Players;
        public GameObject[] Points;

        public PlayerSimple target;
        public GameObject nextPoint;
        public Queue<GameObject> visitedPoints = new Queue<GameObject>(3);

        public BlackBoard(GameObject Holder, string name)
        {
            
            this.Holder = Holder;
            this.targetsInViewRadius = Physics.OverlapSphere(Holder.transform.position, viewRadius, targetMask);
            this.name = name;
            this.Players = GameObject.FindGameObjectsWithTag("Player").Select(o => o.GetComponent<PlayerSimple>()).ToArray<PlayerSimple>();
            this.Points = GameObject.FindGameObjectsWithTag("PatrolPoint").ToArray<GameObject>();
        }

    }
}