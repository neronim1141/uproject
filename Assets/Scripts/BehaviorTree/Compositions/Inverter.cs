﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace behaviorTree
{
    public class Inverter : IBehavior
    {
        IBehavior behavior;


        public Inverter(IBehavior behavior)
        {
            this.behavior = behavior;
        }
        public Status Tick(BlackBoard blackBoard)
        {
            Status s = this.behavior.Tick(blackBoard);
            if (s == Status.Success) return Status.Failure;
            else if (s == Status.Failure) return Status.Success;
            else return s;
        }
    }
}