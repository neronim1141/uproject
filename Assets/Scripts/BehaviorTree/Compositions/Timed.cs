﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace behaviorTree
{
    public class Timed : IBehavior
    {
        private float timeValue;
        public Timed(float timeValue)
        {
            this.timeValue = timeValue;
        }
        public Status Tick(BlackBoard blackBoard)
        {
            Timer newTim = new Timer(timeValue);

            if (!newTim.updateTime(0.0f))
            {
                return Status.Running;
            }
            else if (newTim.getTime())
            {
                return Status.Success;
            }
            else
            {
                return Status.Failure;
            }
        }

    }
}