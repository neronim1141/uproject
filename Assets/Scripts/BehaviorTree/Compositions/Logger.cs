﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using behaviorTree;
    public class Logger : IBehavior
    {
        string msg;

        public Logger(string msg)
        {
            this.msg = msg;
        }

        public Status Tick(BlackBoard blackBoard)
        {
            Debug.Log(this.msg);
            return Status.Success;
        }
    }