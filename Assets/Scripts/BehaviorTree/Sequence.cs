﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace behaviorTree
{
    public class Sequence : Composite
    {
        public override Status Tick(BlackBoard blackBoard)
        {
            for (int i = 0; i < Children.Count; i++)
            {
                Status s = Children[i].Tick(blackBoard);
                if (s == Status.Running) return Status.Running;
                if (s == Status.Failure) return Status.Failure;

            }
            return Status.Success;
        }
        public Sequence(params IBehavior[] behaviors) : base(behaviors)
        {

        }
    }
}