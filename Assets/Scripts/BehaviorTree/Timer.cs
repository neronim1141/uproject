﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer
{
    private float time;
    private float timer;
    public Timer(float time)
    {
        this.time = time;
    }

    public bool updateTime(float deltaTime)
    {
        this.timer = deltaTime;
        deltaTime += Time.deltaTime;
        return getTime();
    }
    public bool getTime()
    {
        if (this.timer >= this.time)
        {
            return true;
        }
        else
        {
            updateTime(this.timer);
        }
        return false;
    }
}
