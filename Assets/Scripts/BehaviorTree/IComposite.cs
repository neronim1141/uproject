﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace behaviorTree
{
    public interface IComposite
    {
        void Add(IBehavior child);
    }
}

