﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace behaviorTree
{
    public class Condition : IBehavior
    {
        public Func<BlackBoard, bool> fn;
        public Condition()
        {

        }
        public Condition(Func<BlackBoard, bool> fn)
        {
            this.fn = fn;
        }
        public Status Tick(BlackBoard blackBoard)
        {
            if (Check(blackBoard)) return Status.Success;
            else return Status.Failure;
        }
        protected virtual bool Check(BlackBoard blackBoard)
        {
            return fn(blackBoard);
        }
    }
}