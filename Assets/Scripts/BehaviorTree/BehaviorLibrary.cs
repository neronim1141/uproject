﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using behaviorTree;
public static class BehaviorLibrary 
{
    #region Drone
    private static IBehavior droneBehavior;
    public static IBehavior DroneBehavior
    {
        get
        {
            if (droneBehavior == null) droneBehavior = InitDroneBehavior();
            return droneBehavior;
        }
    }
    public static IBehavior InitDroneBehavior()
    {
        return new Selector(
               new Sequence(
                        new SeePlayer(),
                        new RemovePatrolPoint(),
                        new ShootDrone()
                    ),
               new Selector(
                    new Sequence(
                        new HasPatrolPoint(),
                        new Selector(
                            new Inverter(
                                new IsNearPatrolPoint(2.0f)
                                ),
                                new RemovePatrolPoint()
                            )

                        ),
                        new Sequence(
                            new GetNewPatrolPoint(),
                            new MoveToPoint()
                            )
                    )



                );
    }
    #endregion
}
