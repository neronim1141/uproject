using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine;
/// <summary>
/// Helper Class
/// </summary>
public static class Extensions{

    /// <summary>
    /// Get one random object from params 
    /// </summary>
    /// <param name="array"> Array type from which you can draw</param>
    /// <typeparam name="TItem"></typeparam>
    /// <returns></returns>
    public static TItem GetRandom<TItem>(this System.Array array)
    {
        TItem[] a=array.Cast<TItem>().ToArray();
        return a[Random.Range(0, a.Length)];
    }

    public static Weight<TItem> GetRandomWithWeights<TItem>(this System.Array array)
    {
             Weight<TItem>[] a = array.Cast<Weight<TItem>>().ToArray();
            float pool= a.Sum(i=>i.weight);
            float randomNum= Random.Range(0,pool);
            float sum=0;
            for(int i=0;i<a.Length;i++){
                sum+=a[i].weight;
                if(sum>=randomNum){
                    return a[i];
                }
            }
           // Debug.Log("Null");
            return null;
    }

    
}