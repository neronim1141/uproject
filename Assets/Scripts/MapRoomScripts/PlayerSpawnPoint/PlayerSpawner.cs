﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    public static List<PlayerSpawner> SpawnPoints = new List<PlayerSpawner>();
    // Start is called before the first frame update
    private void Awake() {
        SpawnPoints.Add(this);
    }

    public Player Spawn(Player playerPrefab){
        Player p= Instantiate(playerPrefab,this.transform.position,this.transform.rotation) as Player;
        SpawnPoints.Remove(this);
        return p;
    }
}
