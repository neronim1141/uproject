using UnityEngine;

[CreateAssetMenu(fileName = "PropSettings", menuName = "MapGenerator/PropSettings", order = 0)]
public class PropSettings : ScriptableObject {

    public Prop[] entities;
    
}