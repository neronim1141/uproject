using UnityEngine;

public class Connector : MonoBehaviour {
    

    public virtual Vector3 toMatch{
		get{
			return transform.forward;
		}
	}

    public void MatchConnectors(Connector newExit)
    {
        //get parent of new Exit
        var newModule = newExit.transform.parent;
        // dalej sie w magiczy sposob przyrownuj� wyj�cia XD
        var correctiveRotation = Azimuth(-toMatch) - Azimuth(newExit.toMatch);
        newModule.RotateAround(newExit.transform.position, Vector3.up, correctiveRotation);

        var correctiveTranslation = transform.position - newExit.transform.position;
        newModule.transform.position += correctiveTranslation;

    }
    private float Azimuth(Vector3 vector)
    {
        return Vector3.Angle(Vector3.forward, vector) * Mathf.Sign(vector.x);
    }

}