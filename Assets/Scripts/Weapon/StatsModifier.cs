﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats
{
    private float min;
    private float max;

    private float flatMod;
    private float percentMod;

   public Stats(float min, float max)
    {
        this.min = min;
        this.max = max;
    }

    public void changeFlatMod(float flatMod)
    {
        this.flatMod = flatMod;
    }

    public void changePercentMod(float percentMod)
    {
        this.percentMod = percentMod;
    }
}
