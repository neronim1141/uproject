﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class Projectile : Spell
{
    public float speed=10;
    public float damage=5;
    // Start is called before the first frame update
    void Start()
    {
        float x = Screen.width / 2f;
        float y = Screen.height / 2f;

        var ray = Camera.main.ScreenPointToRay(new Vector3(x, y, 0));
        GetComponent<Rigidbody>().velocity = ray.direction * speed;
        Destroy(this.gameObject,5);
    }

    private void OnCollisionEnter(Collision collision)
    {
        DamageAble damageAble = collision.gameObject.GetComponent<DamageAble>();
        if (damageAble)
        {
            damageAble.Damage(damage);
            
        }
        Destroy(this.gameObject);
    }
}
