﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellBook : MonoBehaviour
{
    [SerializeField]
    public SpellRecipe[] spells;
    private Dictionary<string,Spell> spellBook= new Dictionary<string, Spell>();
    public string ActiveSpellCode="";
    public Transform spellSpace;
    private Player caster;
    // Start is called before the first frame update
    void Awake()
    { caster=GetComponent<Player>();
        for(int i = 0; i < spells.Length; i++)
        {
            spellBook.Add(spells[i].code, spells[i].spell);

        }
    }

    // Update is called once per frame
    void Update()
    {
       
            if(Input.GetKeyDown(KeyCode.Q))
                ActiveSpellCode="Q"; 
            if (Input.GetKeyDown(KeyCode.E))
                ActiveSpellCode = "E"; 
            if (Input.GetKeyDown(KeyCode.R))
                ActiveSpellCode = "R"; 
            if (Input.GetKeyDown(KeyCode.F))
                ActiveSpellCode = "F"; 
            if (Input.GetKeyDown(KeyCode.Z))
                ActiveSpellCode = "Z";
            if(Input.GetMouseButtonDown(1))
                ActiveSpellCode="";
            if (Input.GetMouseButtonDown(0))
            {
                if (spellBook.ContainsKey(ActiveSpellCode)) { 
                    Spell spell= Instantiate(spellBook[ActiveSpellCode],spellSpace.position,spellSpace.rotation);
                spell.caster=caster;
                    Physics.IgnoreCollision(spell.GetComponent<Collider>(), GetComponent<Collider>());
                    ActiveSpellCode ="";
                }
        }
    }
    [System.Serializable]
    public struct SpellRecipe
    {
        public string code;
        public Spell spell;
    }
}
