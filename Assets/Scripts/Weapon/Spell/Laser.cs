﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(LineRenderer))]
public class Laser : Spell
{
    public float lifeTime=3;
    public float damage=4;
    private LineRenderer laserLine;
    public Material laserMaterial;
    // Start is called before the first frame update
    void Start()
    {
        laserLine = GetComponent<LineRenderer>();
        laserLine.material=laserMaterial;


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if((lifeTime-=Time.deltaTime)<0)
                Destroy(this.gameObject);
            float x = Screen.width / 2f;
            float y = Screen.height / 2f;

            var ray = Camera.main.ScreenPointToRay(new Vector3(x, y, 0));
            laserLine.SetPosition(0,caster.transform.position+caster.transform.forward);
            RaycastHit hit;
            if(Physics.Raycast(ray,out hit, 100))
            {
                laserLine.SetPosition(1,hit.point);
                DamageAble damageAble=hit.transform.gameObject.GetComponent<DamageAble>();
                if (damageAble)
                {
                   damageAble.Damage(damage*Time.deltaTime);
                }
            }
            laserLine.SetPosition(1,ray.GetPoint(100));

        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
